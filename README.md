# Visual Studio Code - Angular Extension Package

<div align="center">
  <a href="https://marketplace.visualstudio.com/items?itemName=Vilton.angular-development-ep">
    <img src="https://angular.dev/assets/images/press-kit/angular_wordmark_gradient.png" alt="angular-logo" width="320px" height="100px"/>
  </a>
</div>

This extension package brings some useful extensions to code using Angular.

## Extensions Included

* [Package - Version Increment](https://marketplace.visualstudio.com/items?itemName=Vilton.version-increment) - The primary goal is to make easier to increase version number that is in package.json file.

* [Angular Snippets](https://marketplace.visualstudio.com/items?itemName=johnpapa.Angular2) - Angular with TypeScript snippets.

* [VSCode Icons](https://marketplace.visualstudio.com/items?itemName=robertohuertasm.vscode-icons) - Icons for VSCode projects.

* [Add to GIT Ignore](https://marketplace.visualstudio.com/items?itemName=maciejdems.add-to-gitignore) - Add to GIT Ignore.

* [Auto Close Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-close-tag) - Automatically add HTML/XML close tag, same as Visual Studio IDE or Sublime Text does.

* [Auto Rename Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag) - Automatically rename paired HTML/XML tag, same as Visual Studio IDE does.

* [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) - Integrates ESLint JavaScript into VS Code.

* [Angular Language Service](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template) - This extension provides a rich editing experience for Angular templates.

* [Docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker) - Docker for Visual Studio Code.

**Happy Coding!**