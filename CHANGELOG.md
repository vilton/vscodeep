# Change Log

All notable changes to the "angular-development-ep" extension pack will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## 1.0.0 - 2024-Jul-21
- Added Package - Version Increment
- Removed Project Version

## 0.0.9 - 2024-Jul-20
- Added Project Version

## 0.0.8 - 2024-Jul-20
- Added ESAngular Language Service
- Added Docker

## 0.0.7 - 2024-Jul-20
- Added ESLint

## 0.0.6 - 2024-Jul-20
- Logo

## 0.0.5 - 2024-Jul-20
- Added Auto Rename Tag

## 0.0.4 - 2024-Jul-20
- Logo

## 0.0.3 - 2024-Jul-20
- Added Auto Close Tag

## 0.0.2 - 2023-Aug-15
- Added Add to GIT Ignore

## 0.0.1 - 2023-Feb-19
- Added vscode-icons

## 0.0.0 - 2023-Feb-18
- Initial release